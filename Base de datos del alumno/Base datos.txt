CREATE DATABASE IF NOT EXISTS admin_flota_espacial;
USE admin_flota_espacial;

/** TABLAS **/


CREATE OR REPLACE TABLE naves(
  cod varchar(9),
  tamaño varchar(30),
  salud int,
  origen text,
  resistencia_ambiental varchar(50),
  capacidad_carga  varchar(30),
  imagen varchar(70),
  PRIMARY KEY(cod)
  );

CREATE OR REPLACE TABLE tripulantes(
  placa varchar(9),
  nombre_apellidos varchar(50),
  especie varchar(30),
  trabajo varchar(30),
  origen text,
  uniforme varchar(70),
  imagen varchar(70),
  PRIMARY KEY(placa)
  );

CREATE OR REPLACE TABLE salas(
  nombre varchar(30),
  actividad text,
  ubicacion varchar(30),
  num_tripulantes int,
  imagen varchar(70),
  cod varchar(9),
  PRIMARY KEY(nombre)
  );

CREATE OR REPLACE TABLE almacen(
  id int AUTO_INCREMENT,
  cod varchar(9),
  cod_almacen varchar(9),
  imagen varchar(70),
  PRIMARY KEY(id)
  );

CREATE OR REPLACE TABLE misiones(
  numero int,
  infome text,
  dificultad varchar(20),
  imagen varchar(70),
  PRIMARY KEY(numero)
  );

CREATE OR REPLACE TABLE medallas(
  id int AUTO_INCREMENT,
  cod varchar(9),
  medallas varchar(30),
  imagen varchar(70),
  PRIMARY KEY(id)
  );

CREATE OR REPLACE TABLE recompensas(
  id int AUTO_INCREMENT,
  numero int,
  recompensas text,
  PRIMARY KEY(id)
  );

CREATE OR REPLACE TABLE puertos(
  nombre varchar(20),
  misiones text,
  tipo_astro varchar(30),
  zona_orbita varchar(30),
  naves_permitidas varchar(50),
  comercio text,
  imagen varchar(70),
  PRIMARY KEY(nombre)
  );

CREATE OR REPLACE TABLE planetas(
  cod_planetas varchar(9),
  nombre varchar(20),
  habitabilidad varchar(50),
  medioambiente text,
  misiones text,
  imagen varchar(70),
  PRIMARY KEY(cod_planetas)
  );

CREATE OR REPLACE TABLE aterrizan(
  id int AUTO_INCREMENT,
  nombre varchar(20),
  cod varchar(9),
  PRIMARY KEY(id)
  );

CREATE OR REPLACE TABLE contratan(
  id int AUTO_INCREMENT,
  cod varchar(9),
  placa varchar(9),
  PRIMARY KEY(id)
  );

CREATE OR REPLACE TABLE exploran(
  id int AUTO_INCREMENT,
  cod varchar(9),
  cod_planetas varchar(9),
  PRIMARY KEY(id)
  );

CREATE OR REPLACE TABLE realizan(
  id int AUTO_INCREMENT,
  cod varchar(9),
  numero int,
  PRIMARY KEY(id)
  );

CREATE OR REPLACE TABLE trabajan(
  id int AUTO_INCREMENT,
  placa varchar(9),
  nombre varchar(30),
  PRIMARY KEY(id)
  );

/** RESTRICCIONES **/

 ALTER TABLE salas
  ADD CONSTRAINT fk_salas_naves
  FOREIGN KEY(cod)
  REFERENCES naves(cod);

ALTER TABLE almacen
    ADD CONSTRAINT fk_almacen_naves
    FOREIGN KEY (cod)
    REFERENCES naves(cod),

    ADD CONSTRAINT uk_almacen_naves
    UNIQUE KEY (cod,cod_almacen);